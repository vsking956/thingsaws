package com.omi.Handler;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.omi.R;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;

public class GetMethod {

    private String url;
    private Context context;

    public GetMethod(String url, Context context) {
        this.url = url;
        this.context = context;
    }

    public void startmethod(final ResponseData ResponseData){

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        StringRequest request = new StringRequest(Request.Method.GET, url,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ResponseData.response(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    JSONObject jsonObject=new JSONObject();
                    jsonObject.put("data",0);
                    jsonObject.put("value","Volley Error");
                    ResponseData.error(error);
                    if(error instanceof NoConnectionError){
                        ConnectivityManager cm =
                                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo activeNetwork = null;
                        if (cm != null) {
                            activeNetwork = cm.getActiveNetworkInfo();
                        }
                        if(activeNetwork != null && activeNetwork.isConnectedOrConnecting()){
                            Toast.makeText(context, "Something went wrong.",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, R.string.device_not_connect_to_internet,
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else if (error instanceof NetworkError || error.getCause() instanceof ConnectException){
                        Toast.makeText(context, R.string.device_not_connect_to_internet,
                                Toast.LENGTH_SHORT).show();
                    } else if (error.getCause() instanceof MalformedURLException){
                        Toast.makeText(context, "Bad Request.", Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ParseError || error.getCause() instanceof IllegalStateException
                            || error.getCause() instanceof JSONException
                            || error.getCause() instanceof XmlPullParserException){
                        Toast.makeText(context, "Parse Error (because of invalid json or xml).",
                                Toast.LENGTH_SHORT).show();
                    } else if (error.getCause() instanceof OutOfMemoryError){
                        Toast.makeText(context, "Out Of Memory Error.", Toast.LENGTH_SHORT).show();
                    }else if (error instanceof AuthFailureError){
                        /*Toast.makeText(context, "server couldn't find the authenticated request.",
                                Toast.LENGTH_SHORT).show();*/
                    } else if (error instanceof ServerError || error.getCause() instanceof ServerError) {
                        Toast.makeText(context, "Server is not responding.", Toast.LENGTH_SHORT).show();
                    }else if (error instanceof TimeoutError || error.getCause() instanceof SocketTimeoutException
                            || error.getCause() instanceof ConnectTimeoutException
                            || error.getCause() instanceof SocketException
                            || (error.getCause().getMessage() != null
                            && error.getCause().getMessage().contains("Connection timed out"))) {
                        Toast.makeText(context, "Connection timeout error",
                                Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }) {
           /* @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String auth = "Bearer " + "ZNaLkJ2S20ean3GfJSzTgFQ3MNlMz3JFBxdejSXs";
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", auth);
                header.put("Content-Type", "application/json");
                return header;
            }*/
        };
        request.setRetryPolicy(new DefaultRetryPolicy( 500*30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }
}
