package com.omi.Handler;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.net.ConnectException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

public class JsonPostRequest {

    private String url;
    private JSONObject params;
    private Context context;
    SharedPreferences sharedPreferences;

    public JsonPostRequest(String url, JSONObject params, Context context) {
        this.url = url;
        this.params = params;
        this.context = context;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

//    ***************callback for post method**********************

    public void startPostMethod(final ResponseData ResponseData) {

        RequestQueue queue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        ResponseData.responseObject(response);

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                ResponseData.error(error);

                try {
                    if (error instanceof NoConnectionError) {
                        ConnectivityManager cm =
                                ((ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE));
                        NetworkInfo activeNetwork = null;
                        if (cm != null) {
                            activeNetwork = cm.getActiveNetworkInfo();
                        }
                        if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {
                            Toast.makeText(context, "Something went wrong.",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "Your device is not connected to internet.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else if (error instanceof NetworkError || error.getCause() instanceof ConnectException) {
                        Toast.makeText(context, "Your device is not connected to internet.",
                                Toast.LENGTH_SHORT).show();
                    } else if (error.getCause() instanceof MalformedURLException) {
                        Toast.makeText(context, "Bad Request.", Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ParseError || error.getCause() instanceof IllegalStateException
                            || error.getCause() instanceof JSONException
                            || error.getCause() instanceof XmlPullParserException) {
                        Toast.makeText(context, "Parse Error (because of invalid json or xml).",
                                Toast.LENGTH_SHORT).show();
                    } else if (error.getCause() instanceof OutOfMemoryError) {
                        Toast.makeText(context, "Out Of Memory Error.", Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ServerError || error.getCause() instanceof ServerError) {
                        Toast.makeText(context, "Server is not responding.", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }) {

            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String auth = "Bearer " + "ZNaLkJ2S20ean3GfJSzTgFQ3MNlMz3JFBxdejSXs";
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", auth);
                header.put("Content-Type", "application/json");
                return header;
            }*/
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(500 * 30000,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);
    }
}
