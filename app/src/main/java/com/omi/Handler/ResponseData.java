package com.omi.Handler;

import com.android.volley.VolleyError;

import org.json.JSONObject;

public interface ResponseData {
    void responseObject(JSONObject data);

    void response(String data);

    void error(VolleyError error);
}
