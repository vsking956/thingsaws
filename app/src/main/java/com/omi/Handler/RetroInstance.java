package com.omi.Handler;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroInstance {
    public static String BASE_URL_SignUp="https://z6kq1qvnug.execute-api.us-west-2.amazonaws.com/omilock/";
    public static String BASE_URL_Login="https://12a4d0gcag.execute-api.us-west-2.amazonaws.com/";
    public static Retrofit retrofit;

    public static Retrofit getRetrofitInstance(){

        HttpLoggingInterceptor interceptor= new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client=new OkHttpClient.Builder().addInterceptor(interceptor).build();

        if (retrofit==null){
            retrofit =new Retrofit.Builder()
                    .baseUrl(BASE_URL_SignUp)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        }
        return retrofit;
    }

    public static Retrofit getRetrofitInstanc_Login(){

        HttpLoggingInterceptor interceptor= new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client=new OkHttpClient.Builder().addInterceptor(interceptor).build();

        if (retrofit==null){
            retrofit =new Retrofit.Builder()
                    .baseUrl(BASE_URL_Login)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        }
        return retrofit;
    }
}
