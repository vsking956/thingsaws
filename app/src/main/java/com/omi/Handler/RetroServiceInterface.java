package com.omi.Handler;


import static com.omi.Handler.Constants.Login;
import static com.omi.Handler.Constants.SIGNUP;

import com.omi.createAccount.CreateUserModel;
import com.omi.dataModel.LoginResponse;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RetroServiceInterface {

    @POST(SIGNUP)
    //@Headers({"Accept:application/json","Content-Type:application/json","Authorization: Bearer"})
    Call<String> createUser(@Body CreateUserModel params);

    @POST(Login)
    Call<Object> userLogin(@Body JSONObject params);

}
