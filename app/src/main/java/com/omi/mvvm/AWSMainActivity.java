package com.omi.mvvm;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.omi.R;
import com.omi.databinding.ActivityAwsmainBinding;

public class AWSMainActivity extends AppCompatActivity {

    ActivityAwsmainBinding activityAwsmainBinding;
    ViewModelAWS viewModelAWS;
    Handler handler = new Handler();
    Runnable runnable;
    int delay = 5000; // 5 second

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityAwsmainBinding = DataBindingUtil.setContentView(this, R.layout.activity_awsmain);
        viewModelAWS = ViewModelProviders.of(this).get(ViewModelAWS.class);
        activityAwsmainBinding.setViewModel(viewModelAWS);
        activityAwsmainBinding.executePendingBindings();

        handler.postDelayed(runnable = () -> {
            handler.postDelayed(runnable, delay);
            // write code
            viewModelAWS.init();
            Log.e("ggggg", "hello");

        }, delay);
    }
}