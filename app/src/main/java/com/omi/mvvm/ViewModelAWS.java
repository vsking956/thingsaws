package com.omi.mvvm;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.VolleyError;
import com.omi.Handler.GetMethod;
import com.omi.Handler.ResponseData;
import com.omi.R;

import org.json.JSONArray;
import org.json.JSONObject;

public class ViewModelAWS extends AWSObservable {
    Context context;
    String first = "first";

    public MutableLiveData<Boolean> fragmentFinishLiveData = new MutableLiveData();


    public ViewModelAWS(@NonNull Application application) {
        super(application);
        this.context = application;
    }

    public void init() {

        if (first.equals("first")) {
            setProgressBarVisibility(View.VISIBLE);
            first = "second";
        } else {
            setProgressBarVisibility(View.GONE);

        }
        new GetMethod("https://sqn7roobg9.execute-api.us-west-2.amazonaws.com/190199/esp", context).startmethod(new ResponseData() {
            @Override
            public void responseObject(JSONObject data) {
            }

            @Override
            public void response(String data) {
                Log.e("Cirtificate_data", data);
                setProgressBarVisibility(View.GONE);
                setMainLayoutVisibility(View.VISIBLE);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    JSONArray jsonArray = jsonObject.getJSONArray("Items");
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                    //setStatus(jsonObject1.getString("statd"));
                    JSONObject jsonObject2 = jsonObject1.getJSONObject("payload");
                    String Status = jsonObject2.optString("STATUS");
                   // setId(jsonObject1.optString("id"));
                   // setBattery(jsonObject1.optString("battery"));
                    if (Status.equalsIgnoreCase("OPEN")) {
                        setStatusColor(context.getResources().getColor(R.color.green));
                        setDeviceImage(context.getResources().getDrawable(R.drawable.device_connected));
                    } else {
                        setStatusColor(context.getResources().getColor(R.color.red));
                        setDeviceImage(context.getResources().getDrawable(R.drawable.disconnected_device));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                setProgressBarVisibility(View.GONE);
                error.printStackTrace();
            }
        });
    }

    public void backFragment() {
        fragmentFinishLiveData.postValue(true);

    }
}
