package com.omi.mvvm;

import android.app.Application;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;

import com.omi.BR;


public class AWSObservable extends ObservableViewModel{

    String status = "" , id = "" , battery = "";

    private int statusColor;
    private Drawable deviceImage;
    private int mainLayoutVisibility = View.GONE;
    private int progressBarVisibility = View.VISIBLE;


    public AWSObservable(@NonNull Application application) {
        super(application);
    }

    @Bindable
    public String getStatus(){
        return status;
    }

    public void setStatus(String status){
        this.status = status;
        notifyPropertyChanged(BR.status);
    }

    @Bindable
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    @Bindable
    public String getBattery() {
        return battery;
    }

    public void setBattery(String battery) {
        this.battery = battery;
        notifyPropertyChanged(BR.battery);
    }

    @Bindable
    public int getStatusColor() {
        return statusColor;
    }

    public void setStatusColor(int statusColor) {
        this.statusColor = statusColor;
        notifyPropertyChanged(BR.statusColor);
    }

    @Bindable
    public int getMainLayoutVisibility() {
        return mainLayoutVisibility;
    }

    public void setMainLayoutVisibility(int mainLayoutVisibility) {
        this.mainLayoutVisibility = mainLayoutVisibility;
        notifyPropertyChanged(BR.mainLayoutVisibility);
    }

    @Bindable
    public int getProgressBarVisibility() {
        return progressBarVisibility;
    }

    public void setProgressBarVisibility(int progressBarVisibility) {
        this.progressBarVisibility = progressBarVisibility;
        notifyPropertyChanged(BR.progressBarVisibility);
    }

    @Bindable
    public Drawable getDeviceImage() {
        return deviceImage;
    }

    public void setDeviceImage(Drawable deviceImage) {
        this.deviceImage = deviceImage;
        notifyPropertyChanged(BR.deviceImage);
    }
}
