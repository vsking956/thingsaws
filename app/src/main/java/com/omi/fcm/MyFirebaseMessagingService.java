package com.omi.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.omi.R;
import com.omi.homeSection.MainActivity;
import com.omi.utils.Common;
import com.omi.utils.SharedPref;

import org.json.JSONObject;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    String message, type, title, text, sound, user_type;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Intent intent;
    private String textMessage = "";
    private final static String TAG = "FCM Message";

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("NEW_TOKEN", s);
        SharedPref.putString(getApplicationContext(), Common.FIREBASE_TOKEN,s);
        /*pref = PreferenceManager.getDefaultSharedPreferences
                (getApplicationContext());
        editor = pref.edit();
        editor.putString("Token", s);
        editor.apply();*/
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            sendNotification(remoteMessage);
            //sendNotification(remoteMessage.getData().get("title"),remoteMessage.getData().get("body"));
            Log.e("User notification1", "" + remoteMessage.getData().get("message"));
            Log.e("User notification", "" + remoteMessage.getData().get("body"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //This method is only generating push notification
    //It is same as we did in earlier posts
    private void sendNotification(RemoteMessage messageBody) {

        Log.d(TAG, "From: " + messageBody.getData().get("body"));
        // Log.d(TAG, "Notification Message Body: " + messageBody.getNotification().getBody());
        try {
            /*JSONObject jsonObject = new JSONObject(messageBody.getData().get("message"));
            JSONObject jsonObject1 = jsonObject.optJSONObject("details");
            type = jsonObject1.optString("notification_type");*/

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "123")
                    .setContentText(messageBody.getData().get("body"))
                    .setContentTitle(messageBody.getData().get("title"))
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setStyle(new NotificationCompat.BigTextStyle())
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setSmallIcon(R.drawable.app_logo_background)
                    //.setContentIntent(pendingIntent)
                    .setAutoCancel(true);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel("123", "Default channel", NotificationManager.IMPORTANCE_HIGH);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.enableVibration(true);
                notificationChannel.canShowBadge();
                notificationChannel.setLockscreenVisibility(RECEIVER_VISIBLE_TO_INSTANT_APPS);
                notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

                assert notificationBuilder != null;
                notificationManager.createNotificationChannel(notificationChannel);
            }

            notificationManager.notify(0, notificationBuilder.build());

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}

