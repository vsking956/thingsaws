package com.omi.interfaceClasses;

public interface ProgressBar {

    void displayProgressDialog();

    void dismissProgressDialog();
}
