package com.omi.interfaceClasses;

public interface Listener {
    void onClick(String msg, String url, String name, String team, String serverPassword, String userPassword, int obj);
}
