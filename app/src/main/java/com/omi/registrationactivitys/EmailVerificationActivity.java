package com.omi.registrationactivitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.omi.R;
import com.omi.homeSection.MainActivity;

public class EmailVerificationActivity extends AppCompatActivity {
    Context context;
    TextView tv_verifyotp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_verification);

        tv_verifyotp = findViewById(R.id.tv_verify_otp);

        tv_verifyotp.setOnClickListener(v -> {
            Intent intent = new Intent(EmailVerificationActivity.this,MainActivity.class);
            startActivity(intent);
            finish();
        });
    }
}