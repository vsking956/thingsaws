package com.omi.registrationactivitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.omi.R;
import com.omi.login.LoginActivity;

public class ForgotActivity extends AppCompatActivity {

    TextView tv_VerifyCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        tv_VerifyCode = findViewById(R.id.tv_verify_code);

        tv_VerifyCode.setOnClickListener(v -> {
            Intent intent  = new Intent(ForgotActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        });
    }
}