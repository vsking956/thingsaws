package com.omi.homeSection;

import android.os.Bundle;
import android.os.Handler;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.omi.R;
import com.omi.fragments.HomeFragment;
import com.omi.fragments.ProfileFragment;
import com.omi.fragments.WifiListFragment;

public class MainActivity extends AppCompatActivity {

    public static ProgressBar progressBar;
    LinearLayout linearMe , linearHome , linear_wifi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearMe = findViewById(R.id.linear_me);
        linearHome = findViewById(R.id.linear_home);
        linear_wifi = findViewById(R.id.linear_wifi);

        linearMe.setOnClickListener(v -> {
            loadFragment(new ProfileFragment());
        });

        linearHome.setOnClickListener(v -> {
            loadFragment(new HomeFragment());
        });

        linear_wifi.setOnClickListener(v -> {
            loadFragment(new WifiListFragment());
        });


        loadFragment(new HomeFragment());

    }

    public void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
       /* Bundle args = new Bundle();
       // args.putString("Category_ID", Category_ID);
        fragment.setArguments(args);*/
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(fragment.getClass().getName());
        transaction.commitAllowingStateLoss();
    }
    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        if (fragment instanceof HomeFragment && fragment.isVisible()) {
            if (doubleBackToExitPressedOnce) {
                finish();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, getResources().getString(R.string.exit_app_msg), Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            getSupportFragmentManager().popBackStackImmediate();
        }
    }
}
