package com.omi.homeSection;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.omi.R;
import com.omi.login.LoginActivity;

public class SetupDeviceActivity extends AppCompatActivity {

    TextView Tv_Setup_device , Tv_SignIn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home1);

        Tv_Setup_device = findViewById(R.id.tv_setup_device);
        Tv_SignIn = findViewById(R.id.tv_signin);

        Tv_Setup_device.setOnClickListener(v -> {
            Intent intent = new Intent(SetupDeviceActivity.this,MainActivity.class);
            startActivity(intent);
            finish();
        });

        Tv_SignIn.setOnClickListener(v -> {
            Intent intent = new Intent(SetupDeviceActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        });
    }
}