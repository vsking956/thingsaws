package com.omi.login;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.omi.R;
import com.omi.createAccount.CreateAccountActivity;
import com.omi.databinding.ActivityLoginBinding;
import com.omi.homeSection.MainActivity;
import com.omi.interfaceClasses.Listener;
import com.omi.interfaceClasses.ProgressBar;
import com.omi.registrationactivitys.ForgotActivity;
import com.omi.utils.AppUtil;

public class LoginActivity extends AppCompatActivity implements Listener, ProgressBar {

    RelativeLayout Btn_newUser;
    LinearLayout Btn_Forgot;
    ActivityLoginBinding binding;
    LoginViewModel viewModel;
    Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_login);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        viewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        binding.setViewModel(viewModel);
        binding.executePendingBindings();
        viewModel.listener = this;
        viewModel.progressBar = this;

    }

    @Override
    public void onClick(String msg, String url, String name, String team, String serverPassword, String userPassword, int obj) {
        Intent i;
        if (msg.equalsIgnoreCase("login")) {
            if (serverPassword.equalsIgnoreCase(userPassword)) {
                i = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            } else {
                Toast.makeText(this, "Wrong Password", Toast.LENGTH_SHORT).show();
            }

        } else if (msg.equalsIgnoreCase("forget")) {
            i = new Intent(LoginActivity.this, ForgotActivity.class);
            startActivity(i);
            finish();
        } else if (msg.equalsIgnoreCase("NewUser")) {
            i = new Intent(LoginActivity.this, CreateAccountActivity.class);
            startActivity(i);
            finish();
        }
    }

    @Override
    public void displayProgressDialog() {
        dialog = AppUtil.showProgress(LoginActivity.this);

    }

    @Override
    public void dismissProgressDialog() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}