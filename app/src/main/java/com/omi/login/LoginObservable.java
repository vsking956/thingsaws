package com.omi.login;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;

import com.omi.mvvm.ObservableViewModel;


public class LoginObservable extends ObservableViewModel {

    private String emailInputText = "", passwordInputText = "";

    public LoginObservable(@NonNull Application application) {
        super(application);
    }

    @Bindable
    public String getEmailInputText() {
        return emailInputText;
    }

    @Bindable
    public void setEmailInputText(String emailInputText) {
        this.emailInputText = emailInputText;
    }

    @Bindable
    public String getPasswordInputText() {
        return passwordInputText;
    }

    @Bindable
    public void setPasswordInputText(String passwordInputText) {
        this.passwordInputText = passwordInputText;
    }
}
