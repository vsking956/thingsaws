package com.omi.login;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.omi.Handler.JsonPostRequest;
import com.omi.Handler.ResponseData;
import com.omi.Handler.RetroInstance;
import com.omi.Handler.RetroServiceInterface;
import com.omi.dataModel.LoginResponse;
import com.omi.interfaceClasses.Listener;
import com.omi.interfaceClasses.ProgressBar;
import com.omi.utils.Common;
import com.omi.utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends LoginObservable {
    Context context;
    Listener listener;
    String userPassword = "";
    ProgressBar progressBar;



    public LoginViewModel(@NonNull Application application) {
        super(application);
        this.context = application;
    }

    /*private void userLogin() {
        RetroServiceInterface retroServiceInterface = RetroInstance.getRetrofitInstanc_Login().create(RetroServiceInterface.class);
        Call<Object> call = retroServiceInterface.userLogin(getRequest());
        Log.e("req",""+getRequest());
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.isSuccessful()) {
                    listener.onClick("login","","","" , "" , "");

                    Log.e("res ", "response --> " + response.body().toString());
                    // dialog.dismiss();

                } else {
                    Log.e("res ", "No response --> ");
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.e("res ", "Excep " + t.getMessage());
            }
        });
    }*/

    private void UserLogin(){
        progressBar.displayProgressDialog();

        try {
            Log.e("login_param", String.valueOf(getRequest()));
            new JsonPostRequest("https://12a4d0gcag.execute-api.us-west-2.amazonaws.com/omilogin", getRequest() , context).startPostMethod(new ResponseData() {
                @Override
                public void responseObject(JSONObject data) {
                    Log.e("jsonlogin_data", "" + data);
                    try{
                        if (data.length() != 0) {
                            JSONObject jsonObject = data.getJSONObject("Item");
                            String phone = jsonObject.optString("Phone");
                            String password = jsonObject.optString("Password");
                            String FirstName = jsonObject.optString("FirstName");
                            String LastName = jsonObject.optString("LastName");
                            String Email = jsonObject.optString("Email");

                            SharedPref.putString(context, Common.MOBILE, phone);
                            SharedPref.putString(context, Common.PASSWORD, password);
                            SharedPref.putString(context, Common.FIRST_NAME, FirstName);
                            SharedPref.putString(context, Common.LAST_NAME, LastName);
                            SharedPref.putString(context, Common.EMAIL, Email);

                            listener.onClick("login", "", "", "", password, userPassword,0);
                            progressBar.dismissProgressDialog();
                        }else {
                            progressBar.dismissProgressDialog();
                            Toast.makeText(context, "Wrong Email Address", Toast.LENGTH_SHORT).show();
                        }

                    }catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void response(String data) {
                    Log.e("login_data", "" + data);
                }

                @Override
                public void error(VolleyError error) {
                    error.printStackTrace();
                    try {
                        String json = new String(error.networkResponse.data, HttpHeaderParser.parseCharset(error.networkResponse.headers));
                        Log.e("===>", json);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private JSONObject getRequest() {
        JSONObject obj = new JSONObject();
        try {
            userPassword = getPasswordInputText();
            obj.put("Email", getEmailInputText());
            obj.put("Password", getPasswordInputText());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

    public void onLoginClick() {
        UserLogin();
    }

    public void newUser_Click(){
        listener.onClick("NewUser", "", "", "", "", "",0);

    }
}
