package com.omi.createAccount;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.omi.Handler.RetroInstance;
import com.omi.Handler.RetroServiceInterface;
import com.omi.interfaceClasses.ProgressBar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateUserViewModel extends ViewModel {
    private MutableLiveData<String> createNewUserLiveData;
    Context context;
     ProgressBar progressBar;

    public CreateUserViewModel() {
        createNewUserLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<String> getCreateNewUserLiveData() {
        return createNewUserLiveData;
    }

    public void setCreateNewUser(CreateUserModel createUserModel) {
        progressBar.displayProgressDialog();
        RetroServiceInterface retroServiceInterface = RetroInstance.getRetrofitInstance().create(RetroServiceInterface.class);

        Call<String> call = retroServiceInterface.createUser(createUserModel);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                   // dialog.dismiss();
                    progressBar.dismissProgressDialog();
                    createNewUserLiveData.postValue(response.body());
                } else {
                    createNewUserLiveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                createNewUserLiveData.postValue(null);
            }
        });
    }

}

