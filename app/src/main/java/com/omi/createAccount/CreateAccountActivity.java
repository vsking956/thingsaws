package com.omi.createAccount;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;

import com.omi.R;
import com.omi.databinding.ActivityCreateAccountBinding;
import com.omi.interfaceClasses.ProgressBar;
import com.omi.login.LoginActivity;
import com.omi.utils.AppUtil;
import com.omi.utils.Common;
import com.omi.utils.SharedPref;

public class CreateAccountActivity extends AppCompatActivity implements ProgressBar {

    ActivityCreateAccountBinding activityCreateAccountBinding;
    TextView Tv_register;
    EditText Et_firstName, Et_lastName, Et_email, Et_mobile, Et_password;
    public MutableLiveData<String> etFirstName = new MutableLiveData<>();

    private CreateUserViewModel createUserViewModel;
    String Firebase_Token = "";
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        Tv_register = findViewById(R.id.tvRegister);
        Et_firstName = findViewById(R.id.etFirstName);
        Et_lastName = findViewById(R.id.etLastName);
        Et_email = findViewById(R.id.etEmail);
        Et_mobile = findViewById(R.id.etMobileNo);
        Et_password = findViewById(R.id.etPassword);

        Firebase_Token = SharedPref.getString(this, Common.FIREBASE_TOKEN);
        Log.e("shared_token==>" , Firebase_Token);

        Tv_register.setOnClickListener(v -> {
           /* Intent intent = new Intent(CreateAccountActivity.this, EmailVerificationActivity.class);
            startActivity(intent);
            finish();*/
            createNewUser();
        });

        createUserViewModel = new ViewModelProvider(this).get(CreateUserViewModel.class);

        createUserViewModel.getCreateNewUserLiveData().observe(this, obj -> {
            Toast.makeText(CreateAccountActivity.this, ""+obj, Toast.LENGTH_SHORT).show();
            Log.e("resp==>" , ""+obj);
            Intent intent = new Intent(CreateAccountActivity.this, LoginActivity.class);
            startActivity(intent);

        });
        createUserViewModel.progressBar = this;

    }

    private void createNewUser() {
        CreateUserModel createUserModel = new CreateUserModel(Et_firstName.getText().toString(),
                Et_lastName.getText().toString(), Et_email.getText().toString(),
                Et_mobile.getText().toString(), Et_password.getText().toString() , Firebase_Token);
        createUserViewModel.setCreateNewUser(createUserModel);
    }

    @Override
    public void displayProgressDialog() {
         dialog = AppUtil.showProgress(CreateAccountActivity.this);
    }

    @Override
    public void dismissProgressDialog() {
        if (dialog.isShowing()){
            dialog.dismiss();
        }
    }
}