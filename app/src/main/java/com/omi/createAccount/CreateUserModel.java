package com.omi.createAccount;

public class CreateUserModel {

    private String FirstName;
    private String LastName;
    private String Email;
    private String Phone;
    private String Password;
    private String Tokenid;

    public CreateUserModel(String firstName, String lastName, String email, String phone, String password , String tokenid) {
        FirstName = firstName;
        LastName = lastName;
        Email = email;
        Phone = phone;
        Password = password;
        Tokenid = tokenid;
    }

    public String getTokenid() {
        return Tokenid;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public String getEmail() {
        return Email;
    }

    public String getPhone() {
        return Phone;
    }

    public String getPassword() {
        return Password;
    }



}
