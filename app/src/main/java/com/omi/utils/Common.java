package com.omi.utils;

public class Common {
    static final String APP_NAME = "Performance";

    //Google login Profile pic
    public static final String CheckTestClick = "CheckTestClick";
    //TestReport_Data
    public static final String USER_ID = "USER_ID";
    public static final String USER_TYPE = "USER_TYPE";
    public static final String FIRST_NAME = "FIRST_NAME";
    public static final String MOBILE = "MOBILE";
    public static final String EMAIL = "EMAIL";
    public static final String OAUTH_UID = "O-AUTH_UID";
    public static final String USER_PIC_FROM_SOCIAL_LOGIN = "USER_PIC_FROM_SOCIAL_LOGIN";
    public static final String CUSTOM_SET_PIC = "CUSTOM_SET_PIC";
    public static final String Total_Score = "Total_Score";
    public static final String AVERAGE = "AVERAGE";
    public static final String LOGIN_CHECK = "LOGIN_CHECK";
    public static final String CHECK_LOGIN_TYPE = "CHECK_LOGIN_TYPE";
    public static final String COUNTRY_CODE = "COUNTRY_CODE";
    public static final String COUNTRY_NAME = "COUNTRY_NAME";
    public static final String CITY_NAME = "CITY_NAME";
    public static final String STATE_NAME = "STATE_NAME";
    public static final String LATITUDE = "LATITUDE";
    public static final String LONGITUDE = "LONGITUDE";
    public static final String LOCALITY = "LOCALITY";
    public static final String ADDRESS = "ADDRESS";
    public static final String REGION = "REGION";
    public static final String NESTED_SCROLL_POS = "NESTED_SCROLL_POS";
    public static final String FIREBASE_TOKEN = "FIREBASE_TOKEN";
    public static final String PASSWORD  = "PASSWORD";
    public static final String LAST_NAME = "LAST_NAME";
}