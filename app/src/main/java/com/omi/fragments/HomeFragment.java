package com.omi.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.omi.R;
import com.omi.adapters.AllDeviceAdapter;
import com.omi.homeSection.MainActivity;
import com.omi.interfaceClasses.Listener;
import com.omi.models.AllDeviceModel;
import com.omi.utils.Common;
import com.omi.utils.SharedPref;

import java.util.ArrayList;

public class HomeFragment extends Fragment implements Listener {

    RecyclerView recyclerView;
    AllDeviceAdapter allDeviceAdapter;
    ArrayList<AllDeviceModel> allDeviceModelArrayList = new ArrayList<>();
    Context context;
    TextView tv_Name;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        context = getActivity();

        recyclerView = view.findViewById(R.id.recycler_device);
        tv_Name = view.findViewById(R.id.tv_namee);

        allDeviceAdapter = new AllDeviceAdapter(context, allDeviceModelArrayList, HomeFragment.this);
        recyclerView.setAdapter(allDeviceAdapter);

        String Fname = SharedPref.getString(context, Common.FIRST_NAME);
        String Lname = SharedPref.getString(context, Common.LAST_NAME);
        tv_Name.setText(Fname + " " + Lname);

        return view;
    }

    @Override
    public void onClick(String msg, String url, String name, String team, String serverPassword, String userPassword, int obj) {

        if (msg.equals("click") && obj == 0) {
            ((MainActivity) context).loadFragment(new DoorStatusFragment());
        } else if (obj == 1) {
            ((MainActivity) context).loadFragment(new LivingAreaDeviceFragment());

        }
    }
}