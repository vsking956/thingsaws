package com.omi.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.omi.R;
import com.omi.databinding.FragmentDoorStatusBinding;
import com.omi.mvvm.ViewModelAWS;


public class DoorStatusFragment extends Fragment {

    ViewModelAWS viewModelAWS;
    Handler handler = new Handler();
    Runnable runnable;
    int delay = 5000; // 5 second
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = getActivity();
        FragmentDoorStatusBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_door_status, container, false);
        viewModelAWS = ViewModelProviders.of(this).get(ViewModelAWS.class);
        binding.setViewModel(viewModelAWS);
        binding.executePendingBindings();

        handler.postDelayed(runnable = () -> {
            handler.postDelayed(runnable, delay);
            // write code
            viewModelAWS.init();
            Log.e("ggggg", "hello");

        }, delay);
        observeData();
        return binding.getRoot();
    }

    private void observeData() {
        viewModelAWS.fragmentFinishLiveData.observe(getActivity(), aBoolean -> {
            if (aBoolean) {
                getActivity().onBackPressed();
            }
        });
    }
}