package com.omi.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.volley.VolleyError;
import com.omi.Handler.GetMethod;
import com.omi.Handler.ResponseData;
import com.omi.R;
import com.omi.databinding.FragmentLivingAreaDeviceBinding;

import org.json.JSONArray;
import org.json.JSONObject;


public class LivingAreaDeviceFragment extends Fragment {


    FragmentLivingAreaDeviceBinding binding;
    Handler handler = new Handler();
    Runnable runnable;
    int delay = 5000; // 5 second
    String first = "first";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_living_area_device, container, false);
        binding.executePendingBindings();

        handler.postDelayed(runnable = () -> {
            handler.postDelayed(runnable, delay);
            // write code
            networkCall();
            Log.e("ggggg", "hello");

        }, delay);



        return binding.getRoot();
    }

    private void networkCall() {
        if (first.equals("first")) {
            binding.loadingBar.setVisibility(View.VISIBLE);
            first = "second";
        } else {
            binding.loadingBar.setVisibility(View.GONE);
        }
        String url = "https://sxxx8wgo29.execute-api.us-west-2.amazonaws.com/190200/esp";
        Log.e("url==>", url);

        new GetMethod(url, getActivity()).startmethod(new ResponseData() {
            @Override
            public void responseObject(JSONObject data) {
            }

            @Override
            public void response(String data) {
                binding.loadingBar.setVisibility(View.GONE);
                binding.rlLayout.setVisibility(View.VISIBLE);

                Log.e("Cirtificate_data", data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    JSONArray jsonArray = jsonObject.getJSONArray("Items");
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                   // setStatus(jsonObject1.getString("statd"));
                    JSONObject jsonObject2 = jsonObject1.getJSONObject("payload");
                    String Status = jsonObject2.optString("STATUS");
                   // setId(jsonObject1.optString("id"));
                  //  setBattery(jsonObject1.optString("battery"));
                    if (Status.equalsIgnoreCase("OPEN")) {
                       // setStatusColor(context.getResources().getColor(R.color.green));
                        binding.ivDevice.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.device_connected));
                    } else {
                      //  setStatusColor(context.getResources().getColor(R.color.red));
                        binding.ivDevice.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.disconnected_device));
                    }
                } catch (Exception e) {
                    binding.loadingBar.setVisibility(View.GONE);
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                binding.loadingBar.setVisibility(View.GONE);

                error.printStackTrace();
            }
        });
    }
}