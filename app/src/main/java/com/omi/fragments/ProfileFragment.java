package com.omi.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.omi.R;
import com.omi.utils.Common;
import com.omi.utils.SharedPref;


public class ProfileFragment extends Fragment {

    TextView tv_Name;
    Context context;

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_profile, container, false);
        context = getActivity();

        tv_Name = view.findViewById(R.id.tv_name);

        String Fname = SharedPref.getString(context, Common.FIRST_NAME);
        String Lname = SharedPref.getString(context, Common.LAST_NAME);
        tv_Name.setText("Hello " +Fname + " " + Lname);

        return view;
    }
}