package com.omi.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.datatransport.runtime.time.TestClock;
import com.omi.R;
import com.omi.interfaceClasses.Listener;
import com.omi.models.AllDeviceModel;

import java.util.ArrayList;


public class AllDeviceAdapter extends RecyclerView.Adapter<AllDeviceAdapter.LeaderHolder> {
    private final Context context;
    private final ArrayList<AllDeviceModel> allDeviceModels;
    private final Listener listener;

    public AllDeviceAdapter(Context context, ArrayList<AllDeviceModel> allDeviceModels,Listener listener) {
        this.context = context;
        this.allDeviceModels = allDeviceModels;
        this.listener = listener;
    }

    @NonNull
    @Override
    public LeaderHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.alldevice_adapter_layout, parent, false);
        return new LeaderHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LeaderHolder holder, int position) {

      /*  AllDeviceModel allDeviceModel = allDeviceModels.get(position);
        String pos = String.valueOf(position + 1);*/

        holder.cardView.setOnClickListener(v -> {
            listener.onClick("click", "" ,
                    "" ,"" , "" , "",position);
        });

        if (position==1){
            holder.tv_test.setText("Living Room");
        }
    }

    @Override
    public int getItemCount() {
        return 6;
        //return dataModelClasses.size();
    }

    public class LeaderHolder extends RecyclerView.ViewHolder {

       CardView cardView;
       TextView tv_test;


        public LeaderHolder(@NonNull View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.card_view);
            tv_test = itemView.findViewById(R.id.tv_text);

        }
    }

}